#include "rf_wifi.h"

#include <WiFi.h>

#include <AutoConnect.h>


AutoConnect g_autoConnect;


void wifiRestartEsp()
{
    ESP.restart();
    for(;;) {
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

void isrPin0()
{
    if(digitalRead(GPIO_NUM_0)==0) {
    }
}

AutoConnect& wifiGetAutoConnect()
{
    return g_autoConnect;
}

void wifiInit()
{
    AutoConnectConfig config;
    config.hostName = getHostname();
    config.apid = getHostname();
    config.autoReconnect = true;
    config.autoSave = AC_SAVECREDENTIAL_AUTO;
    config.autoReset = true;
    config.portalTimeout = 120000;

    g_autoConnect.config(config);
    if(g_autoConnect.begin()) {
        Serial.println("WiFi connected with IP "+ WiFi.localIP().toString());
    } else {
        Serial.println("WiFi could not connected\nRestart node");
        wifiRestartEsp();
    }

    detachInterrupt(GPIO_NUM_0);
    pinMode(GPIO_NUM_0, INPUT_PULLUP);
    delay(20);
    attachInterrupt(GPIO_NUM_0, isrPin0, FALLING);
}

void wifiLoop()
{
    g_autoConnect.handleClient();
    if(WiFi.status() == WL_IDLE_STATUS) {
        wifiRestartEsp();
    }
}

String getHostname()
{
    String macString = WiFi.macAddress();
    macString.replace(":", "");
    macString.toLowerCase();
    macString = "esp-"+macString;

    return macString;
}

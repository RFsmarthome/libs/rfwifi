#ifndef _RF_WIFI_H
#define _RF_WIFI_H

#include <Arduino.h>

class AutoConnect;

extern void wifiLoop();

extern void wifiInit();
extern String getHostname();

extern AutoConnect& wifiGetAutoConnect();

#endif
